﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesignLever_Slider : MonoBehaviour {
	public UnityEngine.UI.Text label;
	public UnityEngine.UI.Text minLabel;
	public UnityEngine.UI.Text maxLabel;
	public UnityEngine.UI.Slider slider;

	private UnityEngine.Events.UnityAction<float> callback;

	private string labelText;
	private float minValue = 0;
	private float maxValue = 10;
	private float width = 10;
	private float value;

	// Handle some rounding to significant digits
	private int significantDigits = 2;

	private void Awake()
	{
		slider.onValueChanged.AddListener(OnSliderValueChanged);
		labelText = label.text;
		minLabel.text = $"{minValue}";
		maxLabel.text = $"{maxValue}";
		UpdateLabel();
	}

	public void SetCallback(UnityEngine.Events.UnityAction<float> call)
	{
		callback = call;
	}

	public void SetLabel(string text)
	{
		labelText = text;
		UpdateLabel();
	}

	public void SetMinMax(float min, float max)
	{
		if (min >= max)
		{
			Debug.LogError("Min MUST be less than max");
			return;
		}
		minLabel.text = $"{min}";
		maxLabel.text = $"{max}";
		minValue = min;
		maxValue = max;

		width = max - min;

		SetValue(value);
	}

	public void SetValue(float val)
	{
		value = Mathf.Min(Mathf.Max(val, minValue), maxValue); // constrain
		FixSignificantDigits();
		var ratio = (value - minValue) / width;
		slider.value = ratio;
		UpdateLabel();
	}

	public void SetValueRatio(float val)
	{
		value = minValue + (maxValue - minValue) * val;
		FixSignificantDigits();
		UpdateLabel();
	}

	public void SetSignificantDigits(int sigdig)
	{
		significantDigits = sigdig;
		FixSignificantDigits();
		UpdateLabel();
	}

	private void OnSliderValueChanged(float val)
	{
		SetValueRatio(val);
		callback?.Invoke(value);
	}

	private void UpdateLabel()
	{
		label.text = $"{labelText} : {value}";
	}

	private void FixSignificantDigits()
	{
		var signif = Mathf.Pow(10, significantDigits);
		float v = (int)(value * signif + 0.5f);
		value = v / signif;
	}
}
