﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesignLever_Toggle : MonoBehaviour {
	public UnityEngine.UI.Text label;
	public UnityEngine.UI.Toggle toggle;

	private UnityEngine.Events.UnityAction<bool> callback;

	private string labelText;
	private bool value;

	// Handle some rounding to significant digits
	private int significantDigits = 2;

	private void Awake()
	{
		toggle.onValueChanged.AddListener(OnToggleValueChanged);
		labelText = label.text;
		UpdateLabel();
	}

	public void SetCallback(UnityEngine.Events.UnityAction<bool> call)
	{
		callback = call;
	}

	public void SetLabel(string text)
	{
		labelText = text;
		UpdateLabel();
	}

	public void SetValue(bool val)
	{
		value = val;
		toggle.isOn = val;
		UpdateLabel();
	}

	private void OnToggleValueChanged(bool val)
	{
		SetValue(val);
		callback?.Invoke(value);
	}

	private void UpdateLabel()
	{
		label.text = $"{labelText} : {value}";
	}
}
