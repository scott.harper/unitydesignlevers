﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class LiveDesignInspector : MonoBehaviour {
	public UnityEngine.UI.Text objectLabelPrefab;
	public UnityEngine.UI.Text behaviourLabelPrefab;
	public DesignLever_Slider sliderPrefab;
	public DesignLever_Toggle togglePrefab;

	public RectTransform contentLocation;

	public GameObject[] objectsToDisplay;

	// Use this for initialization
	void Start () {
		foreach (var go in objectsToDisplay)
		{
			bool addedObjectLabel = false;
			var behaviours = go.GetComponents<MonoBehaviour>();
			foreach (var b in behaviours)
			{
				bool addedBehaviourLabel = false;
				var allFields = b.GetType().GetFields();
				foreach (var field in allFields)
				{
					// Get report on all the DesignLevers in the class
					var attrs = field.GetCustomAttributes(typeof(DesignLever), true) as DesignLever[];
					if (attrs.Length <= 0)
					{
						continue;
					}
					DesignLever lever = attrs[0] as DesignLever;

					if (!addedObjectLabel)
					{
						var label = Instantiate(objectLabelPrefab, contentLocation);
						label.text = go.name;
						addedObjectLabel = true;
					}

					if (!addedBehaviourLabel)
					{
						var label = Instantiate(behaviourLabelPrefab, contentLocation);
						label.text = b.GetType().Name;
						addedBehaviourLabel = true;
					}

					HandleDesignLever(lever, field, b);
				}
			}
		}
	}

	private void HandleDesignLever(DesignLever lever, FieldInfo field, MonoBehaviour b)
	{

		var fieldType = field.FieldType;
		if (fieldType == typeof(float))
		{
			SetupFloatLever(lever, field, b);
		}
		else if (fieldType == typeof(bool))
		{
			SetupBoolLever(lever, field, b);
		}
	}

	private void SetupFloatLever(DesignLever lever, FieldInfo field, MonoBehaviour b)
	{
		DesignLever_Slider slider = Instantiate(sliderPrefab, contentLocation);

		var range = field.GetCustomAttributes(typeof(RangeAttribute), true);
		if (range.Length > 0)
		{
			RangeAttribute r = range[0] as RangeAttribute;
			slider.SetMinMax(r.min, r.max);
		}
		else
		{
			// TODO: find a good order of magnitude based on current value
		}

		slider.SetLabel(field.Name);
		slider.SetValue((float)field.GetValue(b));
		slider.SetSignificantDigits(lever.significantDigits);
		slider.SetCallback((float val) => field.SetValue(b, val));
	}

	private void SetupBoolLever(DesignLever lever, FieldInfo field, MonoBehaviour b)
	{
		DesignLever_Toggle toggle = Instantiate(togglePrefab, contentLocation);
		toggle.SetLabel(field.Name);
		toggle.SetValue((bool)field.GetValue(b));
		toggle.SetCallback((bool val) => field.SetValue(b, val));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
