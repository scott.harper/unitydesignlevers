﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBehaviour : MonoBehaviour {

	[DesignLever(1), Range(1, 7.5f)] public float testFloatValue;
	[DesignLever(-1), Range(0, 200f)] public float testFloat2;
	[DesignLever] public bool testBoolA = true;
	[DesignLever] public bool testBoolB = true;
	[DesignLever] public bool testBoolC = false;
	public float nonTestFloatValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
