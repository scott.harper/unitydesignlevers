﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.AttributeUsage(System.AttributeTargets.Field)]
public class DesignLever : System.Attribute
{
	public int significantDigits;

	public DesignLever(int sigdig = 2)
	{
		significantDigits = sigdig;
	}
}
